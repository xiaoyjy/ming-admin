INSTALL
==============
```
git clone git@bitbucket.org:xiaoyjy/ming-admin.git
git clone git@bitbucket.org:xiaoyjy/cye.git

cd cye
phpize
./configure
make 
make install
```

配置Apache
==============
```
<VirtualHost *:80>

	ServerAlias ming-admin.example.com
        DocumentRoot /my path/www

        RewriteEngine On
        RewriteCond %{DOCUMENT_ROOT}%{REQUEST_FILENAME} !-f
        RewriteCond %{DOCUMENT_ROOT}%{REQUEST_FILENAME} !-d
        RewriteRule . /index.php [L]

        ErrorLog /var/log/apache2/ming-admin-error.log
        CustomLog /var/log/apache2/ming-admin-access.log combined

</VirtualHost>
```

配置log目录
==============
```
mkdir log
chown -R apache:apache log
```
