--TEST--
0x1001_base

network hello.

--FILE--
<?php
$fp = stream_socket_client("udp://127.0.0.1:20000", $errno, $errstr);

$d = [];
$d['name'] = 'houchangcunlu';
$d['size'] = 642498836;
$d['time'] = 1397968732;
$d['md5']  = '2ed2a9066dba34ae5023e8a46b4aa462';
$d['author'] = 'ian';
$d['bounds'] = '40.1,120.1;39.9,120.2';
$d['staff'] = 'ian@autonavi.com';
$s = json_encode($d);

$a = array
(
 [CY_TYPE_UINT16, 0x0005],
 [CY_TYPE_UINT16, 0x1001],
 [CY_TYPE_UINT16, 0x0001],
 [CY_TYPE_UINT8 , 0x0001],
 [CY_TYPE_UINT16, strlen($s)],
 [CY_TYPE_STRING, $s]
 );

$c = array
(
 [CY_TYPE_UINT16, "l1"],
 [CY_TYPE_UINT16, "c1"],
 [CY_TYPE_UINT16, "v1"],
 [CY_TYPE_UINT8 , "f1"],
 [CY_TYPE_UINT16, "l2"],
);

fwrite($fp, cy_pack($a));    
$bin = fread($fp, 1500);
fclose($fp);


var_dump(cy_unpack($bin, $c));

?>
--EXPECT--
array(5) {
  ["l1"]=>
  int(5)
  ["c1"]=>
  int(4097)
  ["v1"]=>
  int(1)
  ["f1"]=>
  int(1)
  ["l2"]=>
  int(104)
}
