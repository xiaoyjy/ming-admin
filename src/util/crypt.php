<?php

define("CRYPT_KEY", "tUb@99*RvLr7#^@T-kq6r9yz");

class CY_Util_Crypt
{
	function encode($txt, $key = CRYPT_KEY)
	{
		return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $txt, MCRYPT_MODE_ECB)); 
	}

	function decode($txt, $key = CRYPT_KEY)
	{
		try
		{
			return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, hex2bin($txt), MCRYPT_MODE_ECB);
		}
		catch(Exception $e)
		{
			return '';
		}
	}
}

?>
