<?php

class CA_Entry_Filters
{
	protected $db;

	function __construct()
	{
		$this->db = new CY_Util_MySQL();
	}

	function edit($id, $req, $env)
	{
		$id      = cy_val($req, 'id'     , 0);
		$site_id = cy_val($req, 'site_id', 0);
		if(empty($id))
		{
			unset($req['id']);
			$r = $this->db->insert('filters', $req);
		}
		else
		{
			$r = $this->db->update('filters', $req, 'id='.(int)$id);
		}

		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
		//return cy_dt(0);
	}

	function enable($id, $req, $env)
	{
		$sql = 'UPDATE `filters` SET enable=1 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}


	function disable($id, $req, $env)
	{
		$sql = 'UPDATE `filters` SET enable=0 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	function del($id, $req, $env)
	{
		$id  = (int)$id;
		$sql = 'UPDATE `filters` SET `status`=99 WHERE `id`='.$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}


}

?>
