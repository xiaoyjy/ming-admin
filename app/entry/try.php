<?php

class CA_Entry_Try
{
	protected $db;

	function __construct()
	{
		$this->db = new CY_Util_MySQL();
	}

	function get($id, $req, $env)
	{
		cy_set_view('try', 'run');
		return cy_dt(0);
	}


	function run($id, $req, $env)
	{
		$url = cy_val($req, 'url', '');
		if(empty($url))
		{
			return cy_dt(0, ['error' => '请输入正确的URL']);
		}

		$parts = parse_url($url);
		$sql   = "SELECT * FROM sites WHERE `host`='".addslashes($parts['host'])."'";
		$r1    = $this->db->query($sql);
		if(empty($r1['data'][0]))
		{
			return cy_dt(0, ['error' => '您还没有添加域名为'.$parts['host'].'的站点']);
		}

		$site  = $r1['data'][0];
		$table_pre = $site['standalone'] == 0 ? '' : $site['name'].'_';
		
		$md5  = md5($url);
		$item = [
			'site_id'=> $site['id'],
			'url'    => $url,
			'md5'    => $md5,
			'host'   => $parts['host'],
			'weight' => 5,
			'timeout'=> 20000,
			];  

		$r2   = $this->db->insert($table_pre."urls", $item, 'ON DUPLICATE KEY UPDATE url=values(url)');
		$sql  = 'SELECT * FROM '.$table_pre."urls WHERE `md5`='".$md5."'"; 
		$r3   = $this->db->query($sql);
		if(empty($r3['data'][0]))
		{
			return cy_dt(0, ['error' => '保存URL失败']);
		}
	
		$proxy= cy_val($req, 'proxy', 0);
		$item = $r3['data'][0];
		$call = 'http://ming.goodoffer.cn/crawler.php?id='.$item['id']."&proxy=".$proxy;
		$curl = new CY_Util_Curl();
                $o = [CURLOPT_USERPWD  => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'],
                      CURLOPT_HTTPAUTH => CURLAUTH_BASIC, 'timeout' => 10000];
		$r = $curl->fetch($call, 'GET', [], ['opt' => $o]);
		if(empty($r))
		{
			return cy_dt(0, ['error' => '接口调用失败']);
		}

		$r = json_decode($r, true);
		if(empty($r['data']['data']))
		{

		}


		return $r;
	}

}

?>
