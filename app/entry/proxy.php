<?php

class CA_Entry_Proxy
{
	function get($id, $req, $env)
	{
		$url  = $_ENV['config']['ming_api'].'/queue.php?name=proxy&cmd=count';
		$curl = new CY_Util_Curl();
		$o = [CURLOPT_USERPWD  => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'],
		      CURLOPT_HTTPAUTH => CURLAUTH_BASIC];

		$r = $curl->fetch($url, 'GET', [], ['opt' => $o]);
		$r = json_decode($r, true);
		return cy_dt(0, ['count' => $r['data']]);
	}

	function clean($id, $req, $env)
	{
		$url  = $_ENV['config']['ming_api'].'/queue.php?name=proxy&cmd=clean';
		$curl = new CY_Util_Curl();
		$o = [CURLOPT_USERPWD  => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'],
		      CURLOPT_HTTPAUTH => CURLAUTH_BASIC];

		$r = $curl->fetch($url, 'GET', [], ['opt' => $o]);
		header("Location: ".$_ENV['url_path'].'/proxy');
		exit;


		$r = json_decode($r, true);
		return cy_dt(0, ['success' => $r['data']]);
	}

}

?>
