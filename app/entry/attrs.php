<?php

class CA_Entry_Attrs
{
	protected $db;

	function __construct()
	{
		$this->db = new CY_Util_MySQL();
	}

	function edit($id, $req, $env)
	{
		$id      = cy_val($req, 'id'     , 0);
		$rule_id = cy_val($req, 'rule_id', 0);
		if(empty($id))
		{
			unset($req['id']);
			$r = $this->db->insert('attrs', $req);
		}
		else
		{
			$r = $this->db->update('attrs', $req, 'id='.(int)$id);
		}

		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
		//return cy_dt(0);
	}

	function enable($id, $req, $env)
	{
		$sql = 'UPDATE `attrs` SET enable=1 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}


	function disable($id, $req, $env)
	{
		$sql = 'UPDATE `attrs` SET enable=0 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	function del($id, $req, $env)
	{
		$id  = (int)$id;
		$sql = 'UPDATE `attrs` SET `status`=99 WHERE `id`='.$id;
		$r   = $this->db->query($sql);
		$sql = 'SELECT rule_id FROM `attrs` WHERE `id`='.$id;
		$r1  = $this->db->query($sql);
		$rule_id = isset($r1['data'][0]['rule_id']) ? $r1['data'][0]['rule_id'] : 0;

		header("Location: ".$_ENV['url_path']."/rules/".$rule_id.'/attrs');
		exit;
	}

}

?>
