<?php

class CA_Entry_Datas
{
        protected $db;

        function __construct()
        {
                $this->db = new CY_Util_MySQL();
        }

	function view($id, $req, $env)
	{
		list($id, $name) = explode('-', $id, 2);

                $sql = 'SELECT * FROM sites WHERE name="'.addslashes($name).'"';
                $r1  = $this->db->query($sql);
                if(empty($r1['data'][0]))
                {
                        return cy_dt(0);
                }
                $site= isset($r1['data'][0]) ? $r1['data'][0] : [];
		$standalone = cy_val($site, 'standalone', 0);
		$table_pre  = $standalone ? addslashes($name).'_' : '';

		$sql = 'SELECT * FROM `'.$table_pre.'datas` WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql); 
		$data= isset($r['data'][0]) ? $r['data'][0] : [];
		if(isset($data['value']))
		{
			$data['value'] = msgpack_unserialize(gzuncompress($data['value'])); 
			return cy_dt(0, ['site' => $site, 'data' => $data]);
		}

		return cy_dt(0, ['site' => $site, 'data' => []]);
	}

	function redo($id, $req, $env)
	{
		list($id, $name) = explode('-', $id, 2);
                $sql = 'SELECT * FROM sites WHERE name="'.addslashes($name).'"';
                $r1  = $this->db->query($sql);
                if(empty($r1['data'][0]))
                {
                        return cy_dt(0);
                }

                $site= isset($r1['data'][0]) ? $r1['data'][0] : [];
		$standalone = cy_val($site, 'standalone', 0);

		$url = $_ENV['config']['ming_api'].'/extract.php?id='.(int)$id;
		if($standalone)
		{
			$url .= '&which='.urlencode($name);
		}

		$curl= new CY_Util_Curl();
		$o   = [CURLOPT_USERPWD => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'], CURLOPT_HTTPAUTH, CURLAUTH_BASIC];
		$r2  = $curl->fetch($url, 'GET', [], ['opt' => $o]);
		$r2  = json_decode($r2, true);
		return cy_dt(0, ['site' => $site, 'data' => $r2]);
	}


	function wget($id, $req, $env)
	{
		list($id, $name) = explode('-', $id, 2);
		$sql = 'SELECT * FROM sites WHERE name="'.addslashes($name).'"';
                $r1  = $this->db->query($sql);
                if(empty($r1['data'][0]))
                {
                        return cy_dt(0);
                }

                $site= isset($r1['data'][0]) ? $r1['data'][0] : [];
		$standalone = cy_val($site, 'standalone', 0);
		$url = $_ENV['config']['ming_api'].'/crawler.php?id='.(int)$id;
		if($standalone)
		{
			$url .= '&which='.urlencode($name);
		}

		if(!empty($req['proxy']))
		{
			$url .= '&proxy=1';
		}

		$curl= new CY_Util_Curl();
		$o   = [CURLOPT_USERPWD => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'], CURLOPT_HTTPAUTH, CURLAUTH_BASIC];
		$r2  = $curl->fetch($url, 'GET', [], ['opt' => $o]);
		$r2  = json_decode($r2, true);
		return cy_dt(0, ['site' => $site, 'data' => $r2]);
	}

}

?>
