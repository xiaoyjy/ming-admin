<?php

class CA_Entry_User
{
	function __call($method, $args)
	{
		return call_user_func_array([$this, 'login'], $args);
	}

	function login($id, $req, $env)
	{
		return cy_dt(0, ['error' => '内部系统，请务必保密']);
	}

	function logout($id, $req, $env)
	{
		setcookie('sid', 'deleted', 0, '/');
		header('Location: '.$_ENV['url_path'].'/user/-/login');
		exit(0);
	}

	function checkin($id, $req, $env)
	{
		//print_r($req);
		if(empty($req['username']) || $req['username'] !== 'admin' ||
		   empty($req['password']) || $req['password'] !== 'ming-pass')
		{
			return cy_dt(0, ['error' => '账号不存在或者密码错误']);
		}

		$time  = time();
		$cert  = CH_LOGIN_CERT;

		$crypt = new CY_Util_Crypt();
		$str   = $crypt->encode($cert.'-'.$time);
		setcookie('sid', $str, $time+86400*30+mt_rand(100, 10000), '/');

		header('Location: '.$_ENV['url_path'].'/sites');
		echo "OK";
		exit(0);
	}

}


?>
