<?php

class CA_Entry_Rules
{
        protected $db;

        function __construct()
        {
                $this->db = new CY_Util_MySQL();
        }

        function edit($id, $req, $env)
        {
                $id      = cy_val($req, 'id'     , 0);
                $site_id = cy_val($req, 'site_id', 0);
		if(empty($id))
		{
			unset($req['id']);
			$r = $this->db->insert('rules', $req);
		}
		else
		{
			$r = $this->db->update('rules', $req, 'id='.(int)$id);
		}

		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
		//return cy_dt(0);
	}

	function attrs($id, $req, $env)
	{
		$sql = 'SELECT * FROM `rules` WHERE id="'.(int)$id.'" AND `status`=0';
		$r1  = $this->db->query($sql);
		if(empty($r1['data'][0]))
		{
			return cy_dt(0, ['rules' => []]);
		}

		$sql = 'SELECT * FROM `attrs` WHERE rule_id="'.(int)$id.'" AND `status`=0';
		$r2  = $this->db->query($sql);
		if(empty($r2['data']))
		{
			return cy_dt(0, ['rules' => $r1['data'][0], 'attrs' => []]);
		}

		return cy_dt(0, ['rules' => $r1['data'][0], 'attrs' => $r2['data']]);
	}

	function tests($id, $req, $env)
	{
		return cy_dt(0);
	}

	function enable($id, $req, $env)
	{
		$sql = 'UPDATE `rules` SET enable=1 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}


	function disable($id, $req, $env)
	{
		$sql = 'UPDATE `rules` SET enable=0 WHERE `id`='.(int)$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}


}

?>
