<?php

class CA_Entry_Sites
{
	protected $db;


	function __construct()
	{
		$this->db = new CY_Util_MySQL();
	}

	function urls($id, $req, $env)
	{
		list($id, $start, $count) = array_pad(explode('-', $id), 3, 0);
		$id   = (int)$id;
		$start= (int)$start;
		$count= $count > 10 ? (int)$count : 10;

		$sql  = "SELECT * FROM sites WHERE id='".$id."'";
		$r1   = $this->db->query($sql);
		$site = isset($r1['data'][0]) ? $r1['data'][0] : [];
		$name = cy_val($site, 'name', '');
		$standalone = cy_val($site, 'standalone', 0);
		$table_pre  = $standalone ? $name.'_' : '';

		$sql  = 'SELECT * FROM `'.$table_pre.'urls` a LEFT JOIN `'
			.$table_pre.'urlstats` b ON a.id=b.id WHERE `site_id`='.(int)$id
			.' ORDER BY a.id DESC LIMIT '.$start.",".$count;
		$r2   = $this->db->query($sql);
		$data = [];
		foreach($r2['data'] as $row)
		{
			$data[] = $row;
		}


		$sql  = "SHOW TABLE STATUS LIKE 'urls'";
		$r3   = $this->db->query($sql);
		$total= isset($r3['data'][0]['Rows']) ? $r3['data'][0]['Rows'] : 0;
		$pages= ['totalPages' => ceil($total/$count), 'currentPage' => ceil(($start+1)/$count)];
		return cy_dt(0, ['site' => $site, 'urls' => $data, 'pages' => $pages]);
	}

	function get($id, $req, $env)
	{
		$sql = 'SELECT * FROM sites WHERE status=0';
		$r   = $this->db->query($sql);
		$data = cy_val($r, 'data', []);

		return cy_dt(0, ['sites' => $data]);
	}

	function edit($id, $req, $env)
        {
		$query = http_build_query($req);

                $url  = $_ENV['config']['ming_api'].'/sites.php?'.$query;
                $curl = new CY_Util_Curl();
                $o = [CURLOPT_USERPWD  => $_ENV['config']['ming_api_user'].":".$_ENV['config']['ming_api_pw'],
                      CURLOPT_HTTPAUTH => CURLAUTH_BASIC];

                $r = $curl->fetch($url, 'GET', [], ['opt' => $o]);
		header("Location: ".$_ENV['url_path']."/sites");
                return cy_dt(0);
        }

	function del($id, $req, $env)
	{
		$id  = (int)$id;
		$sql = 'UPDATE `sites` SET `status`=99 WHERE `id`='.$id;
		$r   = $this->db->query($sql);
		header("Location: ".$_ENV['url_path']."/sites");
		return cy_dt(0);
	}

	function status($id, $req, $env)
	{
		return cy_dt(0);
	}

	function filters($id, $req, $env)
	{
		$id  = (int)$id;
		//$sql='SELECT b.id, b.site, b.rule, b.enable, b.type, b.timeout, b.weight FROM sites a LEFT JOIN filters b ON a.host=b.site WHERE a.id='.$id;
		$sql = 'SELECT * FROM sites WHERE id='.$id;
		$r1  = $this->db->query($sql);
		if(empty($r1['data'][0]))
		{
			return cy_dt(0);
		}

		$site= isset($r1['data'][0]) ? $r1['data'][0] : [];
		$sql = 'SELECT * FROM filters WHERE site_id="'.$id.'" AND `status`=0';
		$r2   = $this->db->query($sql);
		if(empty($r2['data']))
		{
			return cy_dt(0, ['site' => $site, 'filters' => []]);
		}

		return cy_dt(0, ['site' => $site, 'filters' => $r2['data']]);
	}

	function rules($id, $req, $env)
	{
		$id  = (int)$id;
		$sql = 'SELECT * FROM `sites` WHERE id='.$id." AND `status`=0";
		$r1  = $this->db->query($sql);
		if(empty($r1['data'][0]))
		{
			return cy_dt(0);
		}
		$site= isset($r1['data'][0]) ? $r1['data'][0] : [];

		$sql = 'SELECT * FROM `rules` WHERE site_id="'.$id.'" AND `status`=0';
		$r2   = $this->db->query($sql);
                if(empty($r2['data']))
                {
                        return cy_dt(0, ['site' => $site, 'rules' => []]);
                }
		
		return cy_dt(0, ['site' => $site, 'rules' => $r2['data']]);
	}

	function import($id, $req, $env)
	{
		$id  = (int)$id;
		$sql = 'SELECT * FROM `sites` WHERE id='.$id." AND `status`=0";
		$r1  = $this->db->query($sql);
		if(empty($r1['data'][0]))
		{
			return cy_dt(0);
		}
		$site= isset($r1['data'][0]) ? $r1['data'][0] : [];

		return cy_dt(0, ['site' => $site]);
	}
}

?>
