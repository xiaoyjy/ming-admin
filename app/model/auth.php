<?php 

class CA_Model_Auth
{
	function login($token)
	{
		$crypt = new CY_Util_Crypt();
		$str   = $crypt->decode($token);
		list($cert, $time) = array_pad(explode('-', $str), 2, '');

		if(time() - $time < 86400*30 && $cert === CH_LOGIN_CERT)
		{
			return cy_dt(0, true);
		}


		return cy_dt(0, false);
	}

}


?>
