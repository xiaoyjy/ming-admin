<?php

if(!defined("CH_APP_INIT_LOAD")) :

/**
 * @ignore
 * 
 * 防止重入，加ignore，不用记录到文档中 
 */
define("CH_APP_INIT_LOAD", true);
define("CH_LOGIN_CERT", 'Justok');

if(empty($_ENV['img_prefix']))
{
	$_ENV['img_prefix'] = 'http://bijia-ui.oss-cn-qingdao.aliyuncs.com';
}

if(empty($_g_module))
{
	$_g_module = 'index';
} 

function v($string)
{
	echo $string;
}

function v_url($path)
{
	echo $_ENV['url_path'].$path;
}

function v_url_static($path)
{
	echo $_ENV['img_prefix'].$path;
}


if($_g_module !== 'user')
{
	if(isset($_COOKIE['sid']))
	{
		$auth = new CA_Model_Auth();
		$r    = $auth->login($_COOKIE['sid']);
		if(isset($r['data']) && $r['data'] == true)
		{
			$auth_ok = 1;
		}
	}

	if(!isset($auth_ok))
	{
		header('Location: '.$_ENV['url_path'].'/user/-/login');
		exit;
	}
}



endif;

?>
