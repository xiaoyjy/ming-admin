<?php extract($this->data); ?><!DOCTYPE html>
<html lang="en">
<head>
<!--
===
This comment should NOT be removed.

Charisma v2.0.0

Copyright 2012-2014 Muhammad Usman
Licensed under the Apache License v2.0
http://www.apache.org/licenses/LICENSE-2.0

http://usman.it
http://twitter.com/halalit_usman
===
-->
<meta charset="utf-8">
<title>数据抓取管理系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Ming data managment">
<meta name="author" content="Muhammad Usman">

<!-- The styles -->
<link href="<?php v_url_static('/charisma/css/bootstrap-cerulean.min.css');?>" rel="stylesheet">
<link href="<?php v_url_static('/charisma/css/charisma-app.css');?>" rel="stylesheet">


<!-- The fav icon -->
<link rel="shortcut icon" href="<?php v_url_static('/charisma/img/favicon.ico');?>">

</head>

<body>

<div class="ch-container">
<div class="row">
<div class="row">
<div class="col-md-12 center login-header">
<h2>数据抓取管理系统</h2>
</div>

</div> 
<div class="row">
<div class="well col-md-5 center login-box">
<div class="alert alert-info">
<?php echo $data['error'];?>
</div>
<form class="form-horizontal" action="<?php v_url('/user/-/checkin');?>" method="post">
<fieldset>
<div class="input-group input-group-lg">
<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
<input type="text" name='username' class="form-control" placeholder="账号">
</div>
<div class="clearfix"></div><br>
<div class="input-group input-group-lg">
<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
<input type="password" name='password' class="form-control" placeholder="密码">
</div>
<div class="clearfix"></div>

<!--
<div class="input-prepend">
<label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
</div>
-->


<div class="clearfix"></div>
<p class="center col-md-5">
	<button type="submit" class="btn btn-primary">登 录</button>
</p>
</fieldset>
</form>
</div>

</div> 
</div> 
</div> 

</body>
</html>
