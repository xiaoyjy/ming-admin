<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data); ?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php v_url("/");?>">首页</a>
        </li>
        <li>
            <a href="<?php v_url("/sites");?>">站点管理</a>
        </li>
    </ul>
</div>


<div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2>站点列表</h2>
        <div class="box-icon">
            <a href="#" id="site-add" class="btn btn-round btn-default"><i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
    </div>

    <div class="box-content">
        <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>站点名称</th>
                <th>配置</th>
                <th>查看</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>

<?php

foreach($data['sites'] as $item)
{
?>
            <tr id="item_<?php echo $item['id'];?>">
                <td><?php echo $item['id'];?></td>
                <td><a target="_blank" href="<?php echo $item['url'];?>"><?php echo $item['title']." [".$item['name']."]";?></a></td>
                <td class="center">
                    <a href="<?php v_url("/sites/".$item['id']."/filters");?>"> 过滤 </a>
                    <a href="<?php v_url("/sites/".$item['id']."/rules");?>"> 抽取 </a>
                    <a href="<?php v_url("/sites/".$item['id']."/import");?>"> 导入URL </a>
		</td>
                <td class="center">
                    <a href="<?php v_url("/sites/".$item['id']."/status");?>">状态</a>
                    <a href="<?php v_url("/sites/".$item['id']."/urls");?>">数据列表</a>
                </td>

                <td class="center">
                    <a href="javascript:site_edit_show('<?php echo $item['id'];?>', '<?php echo $item['name'];?>', '<?php echo $item['title'];?>', '<?php echo $item['host'];?>', '<?php echo $item['url'];?>');"><div class='glyphicon glyphicon-edit'></div></a>&nbsp;
                    <a href="javascript:site_remove_show('<?php echo $item['id'];?>', '<?php echo $item['name'];?>', '<?php echo $item['title'];?>', '<?php echo $item['host'];?>', '<?php echo $item['url'];?>');"><div class='glyphicon glyphicon-remove'></div></a>

                </td>
            </tr>
<?php

}
?>
            </tbody>
        </table>
<!--
                    <ul class="pagination pagination-centered">
                        <li><a href="#">Prev</a></li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
-->
    </div>
</div>


<div class="modal fade" id="site-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" id='site-editor' role="form" method="POST" action="<?php v_url("/sites/0/edit");?>">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>添加抓取站点</h3>
                </div>


                <div class="modal-body">

  <div class="control-group">
    <label class="control-label" for="inputName">唯一标识</label>
    <input type="text" id="inputName" placeholder="Name" name="name" />
  </div>

  <div class="control-group">
    <label class="control-label" for="inputTitle">站点名称</label>
    <input type="text" id="inputTitle" placeholder="Title" name="title" />
  </div>

  <div class="control-group">
    <label class="control-label" for="inputDomain">站点域名</label>
    <input type="text" id="inputDomain" placeholder="Domain" name="host" />
  </div>

  <div class="control-group">
    <label class="control-label" for="inputURL" class="col-lg-2">入口URL</label>
    <input type="text" id="inputURL" placeholder="URL" name="url" style="width:70%" />
  </div>

  <input name="id" type="hidden" />

                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <input type="submit" class="btn btn-primary" value="保存">
                </div>

            </div>

        </div>
    </form>
</div>


    <div class="modal fade" id="site-remove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>删除站点</h3>
                </div>

                <div class="modal-body" id="remove-note">
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <button id="remove-url" class="btn btn-primary">删除</button>
                </div>
            </div>
        </div>
    </div>

<?php include CY_HOME.'/app/html/patches/footer.php'; ?>


<script type="text/javascript">

    $('#site-add').click(function (e) {
        e.preventDefault();

        $('input[name=id]').val(0);
        $('#site-modal').modal('show');
    });


    $('#site-editor').submit(function(e) {
	return true;
    });    
   
    function site_edit_show(id, name, title, host, url)
    {
	$('#site-editor').find('input[name=name]').val(name);
	$('#site-editor').find('input[name=title]').val(title);
	$('#site-editor').find('input[name=host]').val(host);
	$('#site-editor').find('input[name=id]').val(id);
	$('#site-editor').find('input[name=url]').val(url);
	$('#site-modal').modal('show');
    }

    function site_remove_show(id, name, title, host, url)
    {
	$('#remove-url' ).click(function(e) {
		location.href = '<?php v_url("/sites/"); ?>'+id+'/del';
        });

	$('#remove-note').html("确认删除 " + name + ' / ' + title + ' / ' + host);
	$('#site-remove-modal').modal('show');
    }

</script>

</body>
</html>
