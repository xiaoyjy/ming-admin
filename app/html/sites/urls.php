<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data); ?>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?php v_url("/");?>">首页</a>
		</li>
		<li>
			<a href="<?php v_url("/sites");?>">站点管理</a>
		</li>
		<li>
			数据列表 [<?php echo $data['site']['host']?>]
		</li>
	</ul>
</div>

<div class="box-inner">
	<div class="box-header well" data-original-title="">
		<h2>已抓取数据列表</h2>
		<div class="box-icon">
			<a href="#" id="rule-add" class="btn btn-round btn-default"><i class="glyphicon glyphicon-plus-sign"></i></a>
		</div>
	</div>

	<div class="box-content">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>URL</th>
					<th>下载次数</th>
					<th>失败次数</th>
					<th>返回码</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php
foreach($data['urls'] as $i => $item)
{

?>
			<tr>
				<td><?php echo $item['id'];?></td>
				<td><a href="<?php echo $item['url'];?>" target='_blank'><?php echo htmlspecialchars($item['url']);?></a></td>
				<td><?php echo $item['downloads'];?></td>
				<td><?php echo $item['failures'];?></td>
				<td><?php echo $item['code'];?></td>
				<td>
					<a href="<?php echo v_url('/datas/'.$item['id'].'-'.$data['site']['name'].'/view');?>">查看内容</a>
					<a href="<?php echo v_url('/datas/'.$item['id'].'-'.$data['site']['name'].'/redo');?>">解析</a>
					<a href="<?php echo v_url('/datas/'.$item['id'].'-'.$data['site']['name'].'/wget');?>">抓取</a>
				</td>
			</tr>
<?php
}
?>
			</tbody>
		</table>
	</div>

	<div class="center-block">
		<div id="pagiation"></div>
	</div>

</div>

<div class="modal fade" id="rule-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form class="form-horizontal" id='site-editor' role="form" method="POST" action="<?php v_url("/rules/0/edit");?>">

		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3>编辑规则</h3>
				</div>

				<div class="modal-body">

					<div class="control-group">
						<label style="width:140px" class="control-label" for="inputName">规则</label>
						<span style="margin-left:20px; text-align:left;">
							<input type="text" style="width:60%" id="inputName" placeholder="正则表达式" name="rule" />
						</span>
					</div>

					<div class="control-group">
						<label style="width:140px" class="control-label" for="inputWeight">权重</label>
						<span style="margin-left:20px; text-align:left;">
							<input type="text" id="inputWeight" placeholder="正则表达式" name="weight" value='5' />
						</span>
					</div>

					<div class="control-group">
						<label style="width:140px" class="control-label" for="inputName">规则类型</label>
						<select style="margin-left:20px; text-align:left" id="inputType" name="type">
							<option value=0>列表规则</option>
							<option value=1>内容规则</option>
						</select>
					</div>

					<input name="id" type="hidden" />
					<input name="site_id" type="hidden" value='<?php echo $data['site']['id']?>' />
					<input name="site" type="hidden" value='<?php echo $data['site']['host']?>' />
				</div>

				<div class="modal-footer">
					<a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
					<input type="submit" class="btn btn-primary" value="保存">
				</div>

			</div>

		</div>
	</form>
</div>



<div class="modal fade" id="remove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>删除站点</h3>
			</div>

			<div class="modal-body" id="remove-note">
			</div>

			<div class="modal-footer">
				<a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
				<button id="remove-url" class="btn btn-primary">删除</button>
			</div>
		</div>
	</div>
</div>

<?php include CY_HOME.'/app/html/patches/footer.php'; ?>
<script type='text/javascript'>

var urls = <?php echo json_encode($data['urls']);?>;

$('#rule-add').click(function (e) {
	e.preventDefault();
	$('#rule-modal input[name=id]').val(0);
	$('#rule-modal').modal('show');
});

$('.btn-filter-edit').click(function(e)
{
	e.preventDefault();

	var i = $(this).attr('value');
	var d = urls[i];

	$('#rule-modal form').attr('action', '<?php v_url("/rules/")?>' + d.id + '/edit');
	$('#rule-modal input[name=id]').val(d.id);
	$('#rule-modal input[name=rule]').val(d.rule);
	$('#rule-modal input[name=weight]').val(d.weight);
	$("#rule-modal select[name=type] option[value="+d.type+"]").attr("selected", true);
	$('#rule-modal').modal('show');
});

$('.btn-filter-remove').click(function(e)
{
	e.preventDefault();

	var i = $(this).attr('value');
	var d = urls[i];
	$('#remove-note' ).html ('是否删除, 过滤规则: ' + d.id + ' / ' + d.rule);
	$('#remove-modal').modal('show');
});

$('.btn-filter-enable').click(function(e)
{
	var i = $(this).attr('value');
	var d = urls[i];
	location.href = '<?php v_url("/rules/")?>' + d.id + '/enable';
});

$('.btn-filter-disable').click(function(e)
{
	var i = $(this).attr('value');
	var d = urls[i];
	location.href = '<?php v_url("/rules/")?>' + d.id + '/disable';
});


var options = <?php $pages = isset($data['pages']) ? $data['pages'] : []; echo json_encode($pages);?>;
if(!options.count) options.count = 10;

options.pageUrl = function(type, page, current)
{
	var url = '<?php echo v_url('/sites/'.$data['site']['id'].'-'); ?>' + (page-1)*options.count + '-' + options.count + '/urls';
	return url;
}

$('#pagiation').bootstrapPaginator(options);


</script>



</body>
</html>

