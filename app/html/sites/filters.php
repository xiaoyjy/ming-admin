<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data)?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php v_url("/");?>">首页</a>
        </li>
        <li>
            <a href="<?php v_url("/sites");?>">站点管理</a>
        </li>
	<li>
		网站规则 [<?php echo $data['site']['host']?>]
	</li>
    </ul>
</div>

<div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2>规则列表</h2>
        <div class="box-icon">
            <a href="#" id="rule-add" class="btn btn-round btn-default"><i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
    </div>

    <div class="box-content">
        <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>规则</th>
                <th>类型</th>
                <th>权重</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
<?php
foreach($data['filters'] as $i => $item)
{

?>
           <tr>
                <td><?php echo $item['id'];?></td>
                <td><?php echo htmlspecialchars($item['rule']);?></td>
		<!--
                <td><?php echo ($item['type'] == 0) ? "列表规则" : "<a href='".$_ENV['url_path']."/rules/".$item['site_id']."/attrs'>内容规则</a>";?></td>
		-->
                <td><?php echo ($item['type'] == 0) ? "列表规则" : "内容规则";?></td>
		<td><?php echo $item['weight']?></td>
                <td><?php echo ($item['enable'] == 0) ? '已关闭': '已启用'; ?></td>
                <td><?php echo ($item['enable'] == 0) ?
			"<a value='".$i."' href='##' class='btn-filter-enable'><div class='glyphicon glyphicon-play'></div></a>" :
			"<a value='".$i."' hred='##' class='btn-filter-disable'><div class='glyphicon glyphicon-off'></div></a>";?> &nbsp;
			<a value="<?php echo $i;?>" href="##" class="btn-filter-edit"><div class='glyphicon glyphicon-edit'></div></a>&nbsp;
			<a value="<?php echo $i;?>" href="##" class="btn-filter-remove"><div class='glyphicon glyphicon-remove'></div></a>
                </td>
           </tr>
<?php
}
?>
        </tbody>
        </table>
    </div>

</div>



<div class="modal fade" id="rule-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" id='site-editor' role="form" method="POST" action="<?php v_url("/filters/0/edit");?>">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>编辑规则</h3>
                </div>

                <div class="modal-body">

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputName">规则</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" style="width:60%" id="inputName" placeholder="正则表达式" name="rule" />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputWeight">权重</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" id="inputWeight" placeholder="正则表达式" name="weight" value='5' />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputName">规则类型</label>
                        <select style="margin-left:20px; text-align:left" id="inputType" name="type">
                            <option value=0>列表规则</option>
                            <option value=1>内容规则</option>
                        </select>
                    </div>
			  	
                     <input name="id" type="hidden" />
                     <input name="site_id" type="hidden" value='<?php echo $data['site']['id']?>' />
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <input type="submit" class="btn btn-primary" value="保存">
                </div>

            </div>

        </div>
    </form>
</div>



    <div class="modal fade" id="remove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>删除站点</h3>
                </div>

                <div class="modal-body" id="remove-note">
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <button id="remove-url" class="btn btn-primary">删除</button>
                </div>
            </div>
        </div>
    </div>



<?php include CY_HOME.'/app/html/patches/footer.php'; ?>
<script type='text/javascript'>

    var filters = <?php echo json_encode($data['filters']);?>;

    $('#rule-add').click(function (e) {
        e.preventDefault();
	$('#rule-modal input[name=id]').val(0);
        $('#rule-modal').modal('show');
    });

    $('.btn-filter-edit').click(function(e)
    {
	e.preventDefault();

	var i = $(this).attr('value');
	var d = filters[i];
	$('#rule-modal form').attr('action', '<?php v_url("/filters/")?>' + d.id + '/edit');
	$('#rule-modal input[name=id]').val(d.id);
	$('#rule-modal input[name=rule]').val(d.rule);
	$('#rule-modal input[name=weight]').val(d.weight);
	$("#rule-modal select[name=type] option[value="+d.type+"]").attr("selected", true);
        $('#rule-modal').modal('show');
    });

    $('.btn-filter-remove').click(function(e)
    {
        e.preventDefault();

	var i = $(this).attr('value');
	var d = filters[i];
	$('#remove-note' ).html ('是否删除, 过滤规则: ' + d.id + ' / ' + d.rule);
	$('#remove-url'  ).val('<?php v_url("/filters/"); ?>'+d.id+'/del');
	$('#remove-modal').modal('show');
    });

    $('#remove-url' ).click(function(e) {
	    location.href = $(this).val();
    });

    $('.btn-filter-enable').click(function(e)
    {
	var i = $(this).attr('value');
	var d = filters[i];
	location.href = '<?php v_url("/filters/")?>' + d.id + '/enable';
    });

    $('.btn-filter-disable').click(function(e)
    {
	var i = $(this).attr('value');
	var d = filters[i];
	location.href = '<?php v_url("/filters/")?>' + d.id + '/disable';
    });


</script>



</body>
</html>

