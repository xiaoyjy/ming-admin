<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data); ?>

<div>
	<ul class="breadcrumb">
		<li><a href="<?php v_url("/");?>">首页</a></li>
		<li>抓取测试</li>
	</ul>
</div>

<div class="box-inner">

	<div class="box-header well">
		<h2>测试</h2>

		<div class="box-icon">
		</div>
	</div>

	<?php if(isset($data['error'])) { ?>
        <div class="box-content alerts">
                <div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo $data['error']; ?>
                </div>
        </div>
	<?php } ?>

	<div class="box-content">
		<form action="<?php v_url("/try/0/run");?>">

			<div class="form-group">
				<label for="exampleInputURL">URL</label>
				<input type="text" class="form-control" id="exampleInputURL" name="url" placeholder="Enter URL" value="<?php if(isset($_GET['url'])) echo $_GET['url'];?>">
			</div>

			<div class="form-group">
				<input type="checkbox" name="proxy" value=1 <?php if(isset($_GET['proxy'])) echo "checked"; ?>>使用代理
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary"> 开始测试 </button>
			</div>
		</form>
	</div>


	<div class="box-content">

	</div>

	<div class="box-content">
<?php
if(isset($data['http_code']))
{
	echo "<pre>";
	echo "返回代码: ".$data['http_code']."\n数据详情: \n";
	echo htmlspecialchars($data['data']);
	echo "</pre>";
}
?>
	</div>

</div>


<?php include CY_HOME.'/app/html/patches/footer.php'; ?>

</body>
</html>

