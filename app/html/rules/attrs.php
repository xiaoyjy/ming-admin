<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data); ?>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php v_url("/");?>">首页</a>
        </li>
        <li>
            <a href="<?php v_url("/sites");?>">站点管理</a>
        </li>
	<li>
           <a href="<?php echo $_ENV['url_path']."/sites/".$data['rules']['site_id']."/rules";?>">抽取管理</a>
	</li>
	<li>
           属性列表
	</li>
    </ul>
</div>

<div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2>属性列表</h2>
        <div class="box-icon">
            <a href="#" id="rule-add" class="btn btn-round btn-default"><i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
    </div>

    <div class="box-content">
        <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>名称</th>
                <th>规则</th>
                <th>函数名</th>
                <th>过滤函数</th>
                <th>抽取目标</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
<?php
foreach($data['attrs'] as $i => $item)
{

?>
           <tr>
                <td><?php echo $item['id'];?></td>
                <td><?php echo htmlspecialchars($item['name']);?></td>
                <td>[<b><?php echo htmlspecialchars($item['pattern']);?></b>]
                    <br/>取选第<b><?php echo $item['choice'];?></b>组</td>
                <td><?php echo $item['callback'];?></td>
                <td><?php echo $item['filter'];?></td>

                <td><?php echo $item['source'] == 0 ? '页面' : 'URL';?></td>
                <td><?php echo ($item['enable'] == 0) ? '已关闭': '已启用'; ?></td>
                <td><?php echo ($item['enable'] == 0) ?
			"<a value='".$i."' href='##' class='btn-attr-enable'><div class='glyphicon glyphicon-play'></div></a>" :
			"<a value='".$i."' hred='##' class='btn-attr-disable'><div class='glyphicon glyphicon-off'></div></a>";?> &nbsp;
			<a value="<?php echo $i;?>" href="##" class="btn-attr-edit"><div class='glyphicon glyphicon-edit'></div></a>&nbsp;
			<a value="<?php echo $i;?>" href="##" class="btn-attr-remove"><div class='glyphicon glyphicon-remove'></div></a>
                </td>
           </tr>

<?php
}
?>
        </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="rule-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" id='site-editor' role="form" method="POST" action="<?php v_url("/attrs/0/edit");?>">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>编辑规则</h3>
                </div>

                <div class="modal-body">

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputName">属性名</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" id="inputName" placeholder="" name="name" />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputPattern">规则</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" style="width:60%" id="inputPattern" placeholder="正则表达式" name="pattern" />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputName">选组第几组</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" id="inputName" placeholder="" name="choice" />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputCallback">函数名</label>
                        <span style="margin-left:20px; text-align:left;">
                            <input type="text" style="width:60%" id="inputCallback" placeholder="" name="callback" />
                        </span>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputType">提取方式</label>
                        <select style="margin-left:20px; text-align:left" id="inputType" name="type">
                            <option value=1>正则</option>
                            <option value=2>函数</option>
                            <option value=3>DOM树</option>
                        </select>
                    </div>

                    <div class="control-group">
                        <label style="width:140px" class="control-label" for="inputType1">提取目标</label>
                        <select style="margin-left:20px; text-align:left" id="inputType1" name="source">
                            <option value=0>页面</option>
                            <option value=1>URL</option>
                        </select>
                    </div>

                     <input name="id" type="hidden" />
                     <input name="rule_id" type="hidden" value='<?php echo $data['rules']['id']?>' />
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <input type="submit" class="btn btn-primary" value="保存">
                </div>

            </div>

        </div>
    </form>
</div>


    <div class="modal fade" id="remove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>删除站点</h3>
                </div>

                <div class="modal-body" id="remove-note">
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">取消</a>
                    <button id="remove-url" class="btn btn-primary">删除</button>
                </div>
            </div>
        </div>
    </div>


<?php include CY_HOME.'/app/html/patches/footer.php'; ?>

<script type='text/javascript'>
    var attrs = <?php echo json_encode($data['attrs']);?>;

    $('#rule-add').click(function (e) {
        e.preventDefault();
	$('#rule-modal input[name=id]').val(0);
        $('#rule-modal').modal('show');
    });

    $('.btn-attr-edit').click(function(e)
    {
	e.preventDefault();

	var i = $(this).attr('value');
	var d = attrs[i];

	$('#rule-modal form').attr('action', '<?php v_url("/attrs/")?>' + d.id + '/edit');
	$('#rule-modal input[name=id]').val(d.id);
	$('#rule-modal input[name=pattern]').val(d.pattern);
	$('#rule-modal input[name=name]').val(d.name);
	$('#rule-modal input[name=choice]').val(d.choice);
	$("#rule-modal select[name=type] option[value="+d.type+"]").attr("selected", true);
	$("#rule-modal select[name=source] option[value="+d.source+"]").attr("selected", true);
        $('#rule-modal').modal('show');
    });

    $('.btn-attr-remove').click(function(e)
    {
        e.preventDefault();

	var i = $(this).attr('value');
	var d = attrs[i];
	$('#remove-note' ).html ('是否删除, 过滤规则: ' + d.id + ' / ' + d.pattern + '<input type=hidden name="id" value="'+ d.id +'">');
	$('#remove-modal').modal('show');
    });

    $('.btn-attr-enable').click(function(e)
    {
	var i = $(this).attr('value');
	var d = attrs[i];
	location.href = '<?php v_url("/attrs/")?>' + d.id + '/enable';
    });

    $('.btn-attr-disable').click(function(e)
    {
	var i = $(this).attr('value');
	var d = attrs[i];
	location.href = '<?php v_url("/attrs/")?>' + d.id + '/disable';
    });

    $('#remove-url' ).click(function(e)
    {
	var i = $(this).attr('value');
	var d = attrs[i];
	var id= $('#remove-note input[name=id]').val();
        location.href = '<?php v_url("/attrs/"); ?>'+id+'/del';
    });


</script>


</body>
</html>
