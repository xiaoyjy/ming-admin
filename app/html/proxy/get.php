<?php include CY_HOME.'/app/html/patches/header.php'; ?>
<?php extract($this->data); ?>

<div>
	<ul class="breadcrumb">
		<li><a href="<?php v_url("/");?>">首页</a></li>
		<li>代理查看</li>
	</ul>
</div>

<div class="box-inner">

	<div class="box-header well">
		<h2>查看代理状态</h2>

		<div class="box-icon">
		</div>
	</div>

	<div class="box-content alerts">
		<div class="alert alert-info">
			代理队列中还有 <strong><?php echo $data['count'];?></strong> 个IP
		</div>
	</div>

	<div class="box-content">
		<a class="btn btn-default" href="<?php v_url("proxy/0/clean");?>"> 清空代理 </a>
	</div>
</div>


<?php include CY_HOME.'/app/html/patches/footer.php'; ?>

</body>
</html>

